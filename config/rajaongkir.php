<?php

return [
    'province-data-endpoint' => env('RAJAONGKIR_API_PROVINCE_ENDPOINT', 'https://api.rajaongkir.com/starter/province'),
    'city-data-endpoint' => env('RAJAONGKIR_API_CITY_ENDPOINT', 'https://api.rajaongkir.com/starter/city'),
    'api-key' => env('RAJAONGKIR_API_KEY', 'testapikey12345'),
    'fetch-data-implementation' => env('FETCH_DATA_IMPLEMENTATION', 'local'),
];

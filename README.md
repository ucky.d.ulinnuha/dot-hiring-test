## DOT Hiring Test Project Documentations

Berikut merupakan Dokumentasi dan Tata Cara Instalasi menjalankan project

### Instalasi Project

1. clone project dari repository gitlab
```
git clone https://gitlab.com/ucky.d.ulinnuha/dot-hiring-test.git
```

2. masuk kedalam folder project
```
cd dot-hiring-test
```
3. copy file .env dari .env.example
```
cp .env.example .env
```

4. Instalasi Composer
```
composer install
```

4. Sesuaikan file .env
```
<!-- Pada Bagian Database -->
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=

silahkan isikan DB_DATABASE sesuai nama database yang sudah dibuat,
kemudian DB_USERNAME sesuai dengan nama username layanan database,
dan DB_PASSWORD sesuai dengan password username layanan database anda.


<!-- Pada Bagian RAJAONGKIR -->
silahkan isikan value seperti dibawah ini, 

RAJAONGKIR_API_PROVINCE_ENDPOINT="https://api.rajaongkir.com/starter/province"
RAJAONGKIR_API_CITY_ENDPOINT="https://api.rajaongkir.com/starter/city"
RAJAONGKIR_API_KEY="0df6d5bf733214af6c6644eb8717c92c"

jika anda memiliki API Key tersendiri, silahkan isikan RAJAONGKIR_API_KEY dengan key yang anda miliki
```

5. Generate Key App
silahkan generate key aplikasi dengan menjalankan command dibawah ini :

```
php artisan key:generate
```

6. Jalankan Command `php artisan app:run-test` pada terminal

    Command tersebut akan secara otomatis menjalankan beberapa Tahapan tahapan diantaranya :
   - Migrasi Table yang diperlukan kedalam database yang sudah dibuat
   - Pembuatan Seeder Data untuk keperluan testing
   - Menjalankan Pint Testing (Pengecekan Code yang sudah ditulis sesuai dengan standard PSR PHP)
   - Menjalankan Unit dan Feature testing Feature yang sudah dikembangkan.

7. Jalankan Aplikasi dengan menjalankan command dibawah ini pada terminal :
```
php artisan serve
```

### Informasi Tambahan

- Credential Dummy yang bisa dipakai untuk memeriksa fitur authentikasi adalah :
```
email : test-user1234@mailinator.com
password : user1234
```
- Command untuk Auto Assign Data Regional Provinsi dan Kota yang diambil dari layanan rajaongkir adalah : `php artisan rajaongkir:sync` ( Pastikan Komputer anda terhubung dengan koneksi internet )

- Untuk Implementasi Fitur Swapable Fetch Data Provinsi dan Kota antara Rajaongkir API dan Local Database bisa merubah value `FETCH_DATA_IMPLEMENTATION` pada .env 

```
Jenis Value yang dipakai untuk FETCH_DATA_IMPLEMENTATION pada .env
1. local : fetch data diambil dari database aplikasi
2. rajaongkir-api : fetch data diambil dari layanan rajaongkir (Pastikan Komputer anda terhubung dengan koneksi internet)
```

- Apabila anda ingin melakukan pemeriksaan dokumentasi API Endpoint yang sudah ada melalui platform POSTMAN, silahkan import collection yang sudah disediakan pada folder `docs`

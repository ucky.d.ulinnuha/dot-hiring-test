<?php

namespace App\Http\Services\Features\Authentication;

use Exception;
use Illuminate\Support\Str;

class AuthenticationService
{
    public function loginService($request)
    {
        $inputData = [
            'email' => $request['email'],
            'password' => $request['password'],
        ];

        $attempt = auth()->attempt($inputData);

        if (! $attempt) {
            throw new Exception('Invalid credentials');
        }

        $accessToken = auth()->user()->createToken('access-token-'.$request['email'])->plainTextToken;

        return [
            'access_token' => $accessToken,
        ];
    }

    public function authenticatedUserService()
    {
        return auth()->user();
    }

    public function logoutService()
    {
        $tokenId = Str::before(request()->bearerToken(), '|');

        return auth()->user()->tokens()->where('id', $tokenId)->delete();
    }
}

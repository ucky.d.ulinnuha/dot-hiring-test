<?php

namespace App\Http\Services\Features\Regional;

use App\Http\Repositories\Regional\CityRepository;
use App\Http\Resources\Regional\CityResource;
use App\Traits\RajaOngkirTrait;

class CityService
{
    use RajaOngkirTrait;

    protected $repository;

    public function __construct(CityRepository $repository)
    {
        $this->repository = $repository;
    }

    public function indexService()
    {
        $results = $this->repository->index();

        $fetchDataImplementation = config('rajaongkir.fetch-data-implementation');

        if ($fetchDataImplementation == 'rajaongkir-api') {
            $params = [
                'id' => request('id', null),
                'province' => request('province', null),
            ];

            $results = $this->fetchCityData($params);

            return collect($results);
        }

        return CityResource::collection($results);
    }
}

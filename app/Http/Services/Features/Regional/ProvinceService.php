<?php

namespace App\Http\Services\Features\Regional;

use App\Http\Repositories\Regional\ProvinceRepository;
use App\Http\Resources\Regional\ProvinceResource;
use App\Traits\RajaOngkirTrait;

class ProvinceService
{
    use RajaOngkirTrait;

    protected $repository;

    public function __construct(ProvinceRepository $repository)
    {
        $this->repository = $repository;
    }

    public function indexService()
    {
        $results = $this->repository->index();

        $fetchDataImplementation = config('rajaongkir.fetch-data-implementation');

        if ($fetchDataImplementation == 'rajaongkir-api') {
            $params = request('id', null);

            $results = $this->fetchProvinceData($params);

            return collect($results);
        }

        return ProvinceResource::collection($results);
    }
}

<?php

namespace App\Http\Resources\Regional;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'city_id' => $this->id,
            'city_name' => $this->name,
            'type' => $this->type,
            'postal_code' => $this->postal_code,
            'province_id' => $this->province_id,
            'province_name' => $this->province_name,
        ];
    }
}

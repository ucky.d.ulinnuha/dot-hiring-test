<?php

namespace App\Http\Repositories\Regional;

use App\Http\Repositories\BaseRepository;
use App\Models\City;

class CityRepository extends BaseRepository
{
    protected $model;

    public function __construct(City $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function index()
    {
        return $this->model
            ->search()
            ->orderBy('name', 'asc')
            ->get();
    }
}

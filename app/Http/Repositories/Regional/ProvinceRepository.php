<?php

namespace App\Http\Repositories\Regional;

use App\Http\Repositories\BaseRepository;
use App\Models\Province;

class ProvinceRepository extends BaseRepository
{
    protected $model;

    public function __construct(Province $model)
    {
        parent::__construct($model);
        $this->model = $model;
    }

    public function index()
    {
        return $this->model
            ->search()
            ->orderBy('name', 'asc')
            ->get();
    }
}

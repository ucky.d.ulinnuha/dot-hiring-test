<?php

namespace App\Http\Repositories;

use Illuminate\Database\Eloquent\Model;

class BaseRepository
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function store($inputData)
    {
        return $this->model->create($inputData);
    }

    public function bulkStore($inputData)
    {
        return $this->model->insert($inputData);
    }

    public function findByColumn($column, $params)
    {
        return $this->model->where($column, $params)->first();
    }

    public function getByColumn($column, $params)
    {
        return $this->model->where($column, $params)->get();
    }

    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    public function update($id, $inputData)
    {
        return $this->find($id)->update($inputData);
    }

    public function checkUpdateOrCreate(array $arrayCheck, $inputData)
    {
        return $this->model->updateOrCreate($arrayCheck, $inputData);
    }

    public function destroy($id)
    {
        return $this->find($id)->delete();
    }
}

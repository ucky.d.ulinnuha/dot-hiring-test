<?php

namespace App\Http\Requests\API\Authentication;

use App\Http\Requests\API\BaseRequestValidation;

class LoginRequestValidation extends BaseRequestValidation
{
    public function rules(): array
    {
        return [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ];
    }
}

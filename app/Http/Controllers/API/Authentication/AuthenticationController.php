<?php

namespace App\Http\Controllers\API\Authentication;

use App\Http\Controllers\Controller;
use App\Http\Requests\API\Authentication\LoginRequestValidation;
use App\Http\Services\Features\Authentication\AuthenticationService;

class AuthenticationController extends Controller
{
    protected $service;

    public function __construct(AuthenticationService $service)
    {
        $this->service = $service;
    }

    public function login(LoginRequestValidation $request)
    {
        $inputData = $request->validated();

        try {
            $authenticatedUserToken = $this->service->loginService($inputData);

            return $this->resultResponse('success', 200, 'Successfully Logged In', $authenticatedUserToken);
        } catch (\Throwable $th) {
            return $this->resultResponse('failed', 400, $th->getMessage());
        }
    }

    public function currentUser()
    {
        try {
            $currentUser = $this->service->authenticatedUserService();

            return $this->resultResponse('success', 200, 'Successfully Fetched Current User Profile', $currentUser);
        } catch (\Throwable $th) {
            return $this->resultResponse('failed', 400, $th->getMessage());
        }
    }

    public function logout()
    {
        try {
            $this->service->logoutService();

            return $this->resultResponse('success', 200, 'Successfully Logged Out');
        } catch (\Throwable $th) {
            return $this->resultResponse('failed', 400, $th->getMessage());
        }
    }
}

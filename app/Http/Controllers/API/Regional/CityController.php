<?php

namespace App\Http\Controllers\API\Regional;

use App\Http\Controllers\Controller;
use App\Http\Services\Features\Regional\CityService;

class CityController extends Controller
{
    protected $service;

    public function __construct(CityService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        try {
            $cities = $this->service->indexService();

            $message = 'Successfully Fetched City Data With '.config('rajaongkir.fetch-data-implementation').' Data Implementation';

            return $this->resultResponse('success', 200, $message, $cities);
        } catch (\Throwable $th) {
            return $this->resultResponse('failed', 500, $th->getMessage());
        }
    }
}

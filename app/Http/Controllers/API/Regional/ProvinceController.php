<?php

namespace App\Http\Controllers\API\Regional;

use App\Http\Controllers\Controller;
use App\Http\Services\Features\Regional\ProvinceService;

class ProvinceController extends Controller
{
    protected $service;

    public function __construct(ProvinceService $service)
    {
        $this->service = $service;
    }

    public function index()
    {
        try {
            $provinces = $this->service->indexService();

            $message = 'Successfully Fetched Province Data With '.config('rajaongkir.fetch-data-implementation').' Data Implementation';

            return $this->resultResponse('success', 200, $message, $provinces);
        } catch (\Throwable $th) {
            return $this->resultResponse(
                'failed',
                500,
                $th->getMessage()
            );
        }
    }
}

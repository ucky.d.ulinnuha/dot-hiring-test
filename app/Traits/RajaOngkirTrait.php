<?php

namespace App\Traits;

use Illuminate\Support\Facades\Http;

trait RajaOngkirTrait
{
    protected function baseFetchHttpResponse($endpoint, $params = null)
    {
        $fetchData = Http::withHeaders([
            'key' => config('rajaongkir.api-key'),
        ])->get($endpoint, $params);

        return $fetchData['rajaongkir']['results'];
    }

    public function fetchProvinceData($params = null)
    {
        if (isset($params)) {
            $params = [
                'id' => $params['id'],
            ];
        }

        return $this->baseFetchHttpResponse(config('rajaongkir.province-data-endpoint'), $params);
    }

    public function fetchCityData($params = null)
    {
        if (isset($params)) {
            $params = [
                'id' => $params['id'],
                'province' => $params['province'],
            ];
        }

        return $this->baseFetchHttpResponse(config('rajaongkir.city-data-endpoint'), $params);
    }
}

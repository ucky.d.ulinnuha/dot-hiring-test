<?php

namespace App\Traits;

trait ApiResponseTrait
{
    public function resultResponse(
        string $status,
        int $code,
        string $message,
        $data = null
    ) {
        $result = [
            'meta' => [
                'status' => $status,
                'code' => $code,
                'message' => $message,
            ],
        ];

        if ($data) {
            $result = array_merge($result, [
                'data' => $data,
            ]);
        }

        return response()->json($result, $code);
    }
}

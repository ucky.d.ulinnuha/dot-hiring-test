<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class City extends Model
{
    use SoftDeletes;

    protected $table = 'cities';

    protected $fillable = [
        'province_id',
        'name',
        'type',
        'postal_code',
    ];

    public function province()
    {
        return $this->belongsTo(Province::class);
    }

    public function scopeSearch($query)
    {
        $params = [
            'id' => request('id', null),
            'province' => request('province', null),
        ];

        if ($params['id'] || $params['province']) {
            return $query->where('id', $params['id'])
                ->orWhere('province_id', $params['province']);
        }

        return $query;
    }

    public function getProvinceNameAttribute()
    {
        return optional($this->province)->name ?? '-';
    }
}

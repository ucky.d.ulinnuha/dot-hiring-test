<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RunTestCommand extends Command
{
    /**
     * Name : Running Test Command And Migration.
     *
     * @var string
     */
    protected $signature = 'app:run-test';

    /**
     * Name : Running Test Command And Migration.
     *
     * @var string
     */
    protected $description = 'Running Test Command And Migration';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {

            $composerDump = shell_exec('composer dump-autoload -o');

            $this->info($composerDump);

            $pintTest = shell_exec('./vendor/bin/pint');

            $this->info($pintTest);

            $this->call('migrate:refresh');

            $this->call('db:seed');

            $test = shell_exec('php artisan test --stop-on-failure --log-junit=storage/logs/test-result.log');

            $this->info($test);
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
        }
    }
}

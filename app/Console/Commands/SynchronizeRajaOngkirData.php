<?php

namespace App\Console\Commands;

use App\Http\Repositories\Regional\CityRepository;
use App\Http\Repositories\Regional\ProvinceRepository;
use App\Traits\RajaOngkirTrait;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class SynchronizeRajaOngkirData extends Command
{
    use RajaOngkirTrait;

    protected $provinceRepository;

    protected $cityRepository;

    public function __construct(ProvinceRepository $provinceRepository, CityRepository $cityRepository)
    {
        parent::__construct();
        $this->cityRepository = $cityRepository;
        $this->provinceRepository = $provinceRepository;
    }

    /**
     * Command Name : Synchronize RajaOngkir Data
     * Purpose : To synchronize RajaOngkir data to Application database
     *
     * @var string
     */
    protected $signature = 'rajaongkir:sync';

    /**
     * Command Name : Synchronize RajaOngkir Data
     * Purpose : To synchronize RajaOngkir data to Application database
     *
     * @var string
     */
    protected $description = 'Synchronizing RajaOngkir data to Application database';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        try {
            DB::transaction(function () {

                /**
                 * Fetch Province Data
                 */
                $provinces = $this->fetchProvinceData();

                foreach ($provinces as $province) {
                    $params = [
                        'id' => $province['province_id'],
                    ];

                    $inputData = [
                        'id' => $province['province_id'],
                        'name' => $province['province'],
                    ];

                    $this->provinceRepository->checkUpdateOrCreate($params, $inputData);
                }

                /**
                 * Fetch City Data
                 */
                $cities = $this->fetchCityData();

                foreach ($cities as $city) {
                    $params = [
                        'id' => $city['city_id'],
                        'province_id' => $city['province_id'],
                    ];

                    $inputData = [
                        'id' => $city['city_id'],
                        'name' => $city['city_name'],
                        'province_id' => $city['province_id'],
                        'type' => $city['type'],
                        'postal_code' => $city['postal_code'],
                    ];

                    $this->cityRepository->checkUpdateOrCreate($params, $inputData);
                }
            });

            return $this->info('Successfully synchronize RajaOngkir data to Database');
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
        }
    }
}

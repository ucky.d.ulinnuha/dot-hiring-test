<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Province;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RegionalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::transaction(function () {
            $fetchProvinces = file_get_contents(storage_path('dummy-data/rajaongkir-api-province-response.json'));

            $decodedProvinceData = json_decode($fetchProvinces, true);

            $provinces = $decodedProvinceData['rajaongkir']['results'];

            foreach ($provinces as $province) {
                Province::create([
                    'id' => $province['province_id'],
                    'name' => $province['province'],
                ]);
            }

            $fetchCities = file_get_contents(storage_path('dummy-data/rajaongkir-api-city-response.json'));

            $decodedCityData = json_decode($fetchCities, true);

            $cities = $decodedCityData['rajaongkir']['results'];

            foreach ($cities as $city) {
                City::create([
                    'id' => $city['city_id'],
                    'province_id' => $city['province_id'],
                    'type' => $city['type'],
                    'name' => $city['city_name'],
                    'postal_code' => $city['postal_code'],
                ]);
            }
        });
    }
}

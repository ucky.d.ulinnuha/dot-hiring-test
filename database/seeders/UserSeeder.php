<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = [
            'name' => 'Test User',
            'email' => 'test-user1234@mailinator.com',
            'password' => bcrypt('user1234'),
            'email_verified_at' => now(),
        ];

        User::create($user);
    }
}

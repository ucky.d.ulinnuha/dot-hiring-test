<?php

namespace Tests\Feature;

use Tests\TestCase;

class CityFeatureTests extends TestCase
{
    protected function loginHandler($credentials = [])
    {
        $response = $this->postJson(route('api.auth.login'), $credentials);

        return $response;
    }

    protected function logoutHandler($credentials = [])
    {
        $authenticatedResponse = $this->loginHandler($credentials);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$authenticatedResponse->json('data.access_token'),
        ])->getJson(route('api.auth.logout'));

        return $response;
    }

    protected function fetchCity($params = null)
    {
        $credentials = [
            'email' => 'test-user1234@mailinator.com',
            'password' => 'user1234',
        ];

        $authenticatedResponse = $this->loginHandler($credentials);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$authenticatedResponse->json('data.access_token'),
        ])
            ->getJson(route('api.city.index', $params));

        return $response;
    }

    /** @test */
    public function user_successfully_fetched_cities_data_with_local_data_fetch_implementation()
    {
        $response = $this->fetchCity();

        $response->assertStatus(200);

        $results = $response->json();

        $this->assertNotEmpty($results['data']);
    }

    /** @test */
    public function user_successfully_fetched_cities_data_with_rajaongkir_api_data_fetch_implementation()
    {
        putenv('FETCH_DATA_IMPLEMENTATION=rajaongkir-api');

        $response = $this->fetchCity();

        $response->assertStatus(200);

        $results = $response->json();

        $this->assertNotEmpty($results['data']);

        putenv('FETCH_DATA_IMPLEMENTATION=local');
    }

    /** @test */
    public function user_successfully_fetched_city_data_with_specific_parameter()
    {
        $response = $this->fetchCity([
            'id' => '1',
            'province' => '17',
        ]);

        $response->assertStatus(200);

        $results = $response->json('data');

        $this->assertEquals('Nanggroe Aceh Darussalam (NAD)', $results[0]['province_name']);
        $this->assertEquals('Aceh Barat', $results[0]['city_name']);
    }

    /** @test */
    public function failed_fetched_city_data_without_authorization_access()
    {
        $response = $this->getJson(route('api.city.index'));

        $response->assertStatus(401);
    }
}

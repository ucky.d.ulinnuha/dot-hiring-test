<?php

namespace Tests\Feature;

use Tests\TestCase;

class AuthenticationFeatureTests extends TestCase
{
    protected function loginHandler($credentials = [])
    {
        $response = $this->postJson(route('api.auth.login'), $credentials);

        return $response;
    }

    protected function logoutHandler($credentials = [])
    {
        $authenticatedResponse = $this->loginHandler($credentials);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$authenticatedResponse->json('data.access_token'),
        ])->getJson(route('api.auth.logout'));

        return $response;
    }

    /** @test */
    public function email_field_is_required()
    {
        $response = $this->postJson(route('api.auth.login'), []);

        $response->assertStatus(400);
    }

    /** @test */
    public function email_field_must_be_email_type_format()
    {
        $response = $this->postJson(route('api.auth.login'), [
            'email' => 'test-email',
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function email_field_must_be_existed()
    {
        $response = $this->postJson(route('api.auth.login'), [
            'email' => 'test-email@mailinator.com',
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function password_field_is_required()
    {
        $response = $this->postJson(route('api.auth.login'), [
            'email' => 'test-user1234@mailinator.com',
        ]);

        $response->assertStatus(400);
    }

    /** @test */
    public function user_successfully_logged_in_into_application()
    {
        $credentials = [
            'email' => 'test-user1234@mailinator.com',
            'password' => 'user1234',
        ];

        $response = $this->postJson(route('api.auth.login'), $credentials);

        $response->assertStatus(200);

        $this->logoutHandler($credentials);
    }

    /** @test */
    public function user_successfully_logged_out_from_application()
    {
        $credentials = [
            'email' => 'test-user1234@mailinator.com',
            'password' => 'user1234',
        ];

        $response = $this->logoutHandler($credentials);

        $response->assertStatus(200);
    }
}

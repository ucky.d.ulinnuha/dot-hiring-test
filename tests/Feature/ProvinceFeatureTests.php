<?php

namespace Tests\Feature;

use Tests\TestCase;

class ProvinceFeatureTests extends TestCase
{
    protected function loginHandler($credentials = [])
    {
        $response = $this->postJson(route('api.auth.login'), $credentials);

        return $response;
    }

    protected function logoutHandler($credentials = [])
    {
        $authenticatedResponse = $this->loginHandler($credentials);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$authenticatedResponse->json('data.access_token'),
        ])->getJson(route('api.auth.logout'));

        return $response;
    }

    protected function fetchProvince($params = null)
    {
        $credentials = [
            'email' => 'test-user1234@mailinator.com',
            'password' => 'user1234',
        ];

        $authenticatedResponse = $this->loginHandler($credentials);

        $response = $this->withHeaders([
            'Authorization' => 'Bearer '.$authenticatedResponse->json('data.access_token'),
        ])
            ->getJson(route('api.province.index', $params));

        return $response;
    }

    /** @test */
    public function user_successfully_fetched_provinces_data_with_local_data_fetch_implementation()
    {
        $response = $this->fetchProvince();

        $response->assertStatus(200);

        $results = $response->json();

        $this->assertNotEmpty($results['data']);

        $this->assertEquals(
            'Successfully Fetched Province Data With local Data Implementation',
            $results['meta']['message']
        );
    }

    /** @test */
    public function user_successfully_fetched_provinces_data_with_rajaongkir_api_data_fetch_implementation()
    {
        putenv('FETCH_DATA_IMPLEMENTATION=rajaongkir-api');

        $response = $this->fetchProvince();

        $response->assertStatus(200);

        $results = $response->json();

        $this->assertNotEmpty($results['data']);

        $this->assertEquals(
            'Successfully Fetched Province Data With local Data Implementation',
            $results['meta']['message']
        );

        putenv('FETCH_DATA_IMPLEMENTATION=local');
    }

    /** @test */
    public function user_successfully_fetched_province_data_with_specific_parameter()
    {
        $response = $this->fetchProvince([
            'id' => '1',
        ]);

        $response->assertStatus(200);

        $results = $response->json('data');

        $this->assertEquals('Bali', $results[0]['province']);
    }

    /** @test */
    public function failed_fetched_province_data_without_authorization_access()
    {
        $response = $this->getJson(route('api.province.index'));

        $response->assertStatus(401);
    }
}

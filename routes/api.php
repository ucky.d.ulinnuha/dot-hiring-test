<?php

use App\Http\Controllers\API\Authentication\AuthenticationController;
use App\Http\Controllers\API\Regional\CityController;
use App\Http\Controllers\API\Regional\ProvinceController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('login', [AuthenticationController::class, 'login'])
        ->name('api.auth.login');
});

Route::middleware('auth:sanctum')->group(function () {
    Route::get('logout', [AuthenticationController::class, 'logout'])
        ->name('api.auth.logout');

    Route::get('current-user-profile', [AuthenticationController::class, 'currentUser'])
        ->name('api.auth.current-user-profile');

    Route::prefix('search')->group(function () {
        Route::get('province', [ProvinceController::class, 'index'])
            ->name('api.province.index');

        Route::get('city', [CityController::class, 'index'])
            ->name('api.city.index');
    });
});
